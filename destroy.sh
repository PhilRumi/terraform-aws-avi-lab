#!/bin/bash

environment_name="cisco-bootcamp"

for i in 1
do
  make destroy environment=$environment_name-$i &
  make destroy environment=$environment_name-$(expr $i + 1) &
  make destroy environment=$environment_name-$(expr $i + 2) &
  make destroy environment=$environment_name-$(expr $i + 3) &
  make destroy environment=$environment_name-$(expr $i + 4) &
  wait
done