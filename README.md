# Avi AWS Lab Build Template

## Prereqs

Install terraform and ansible and make sure you have the avi config role installed: https://github.com/avinetworks/ansible-role-aviconfig. Also make sure you have prereqs (curl, jq) installed for https://github.com/timarenz/terraform-aws-avicontroller.

## Usage

First initialize the environment using `make init`.

After this is done run `make` or `make apply` using the parameter "environment=name". The name can be anything you like, shouldn't be too long.

Example: ``` make environment=my-lab ```

This will apply the terraform configuration and deploy the controller, some perf server clients and so on.

To destroy the environment run `make destroy environment=my-lab`

## Build multiple environments
Use `make.sh` and `destroy.sh`as an example on how to build/destroy multiple environments at once.

## Caveats
You might have to delete the environment manually if you do not unconfigure the cloud configuration in the controller as terraform doesn't know about the services engines and will fail to destroy the environment as unkown objects are in it.