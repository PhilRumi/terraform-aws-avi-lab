provider "aws" {
  shared_credentials_file = "${var.credentials}"
  region     = "${var.region}"
}

# lab 1
module "vpc" {
  source           = "github.com/timarenz/terraform-aws-vpc"
  environment_name = "${var.environment}"
  vpc_tags {
    Owner = "${var.owner}"
    Dept  = "${var.dept}"
    Role  = "${var.role}"
  }
  public_subnet_tags {
    Owner = "${var.owner}"
    Dept  = "${var.dept}"
    Role  = "${var.role}"
  }
  private_subnet_tags {
    Owner = "${var.owner}"
    Dept  = "${var.dept}"
    Role  = "${var.role}"
  }
}

output "vpc_id" {
  value = "${module.vpc.vpc_id}"
}
output "tags" {
   value = "${module.vpc.vpc_tags}"
}
output "vpc_name" {
  value = "${module.vpc.vpc_name}"
}

output "private_subnet_name" {
  value = "${module.vpc.vpc_private_subnet_name}"
}

output "private_subnet_az" {
  value = "${module.vpc.vpc_private_subnet_az}"
}

output "public_subnet_name" {
  value = "${module.vpc.vpc_public_subnet_name}"
}

output "public_subnet_az" {
  value = "${module.vpc.vpc_public_subnet_az}"
}

module "avicontroller" {
  source = "github.com/timarenz/terraform-aws-avicontroller"

  public_key = "${file("lab.key.pub")}"

  ami_id         = "ami-0f65be129df4e7e00" #18.2.2

  subnet_id        = "${module.vpc.vpc_public_subnet_id}"
  password         = "${var.password}"
  environment_name = "${var.environment}"
}

output "avicontroller_public_ip" {
  value = "${module.avicontroller.public_ip}"
}

module "perf_server_client" {
 # source            = "gitlab.com/oivan/terraform-aws-perf-server-client"
  source            = "git::ssh://git@gitlab.com/oivan/terraform-aws-perf-server-client.git" 
  public_subnet_id  = "${module.vpc.vpc_public_subnet_id}"
  private_subnet_id = "${module.vpc.vpc_private_subnet_id}"
  environment_name  = "${var.environment}"
  ami_id	    = "${var.ami}"
  owner             = "${var.owner}"
  dept              = "${var.dept}"
  role              = "${var.role}"
}

output "client_public_ip" {
  value = "${module.perf_server_client.client_public_ip}"
}

resource "null_resource" "avi_cloud" {
  depends_on = ["module.avicontroller"]

  provisioner "local-exec" {
    command = "ansible-playbook files/configure-cloud.yml --extra-vars 'region=${var.region} avi_controller=${module.avicontroller.public_ip[0]} avi_username=admin avi_password=${var.password} vpc_name=${module.vpc.vpc_name} vpc_id=${module.vpc.vpc_id} private_subnet_az=${module.vpc.vpc_private_subnet_az} private_subnet_name=${module.vpc.vpc_private_subnet_name} owner=${var.owner} role=${var.role} dept=${var.dept}'"
  }
}
